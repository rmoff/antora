= What's New in Antora {page-component-version}
:description: The new features, changes, and bug fixes included in Antora {page-component-version} and its patch releases.
:doctype: book
:route: New
//:page-toclevels: 0
:leveloffset: 1
:url-releases-asciidoctor: https://github.com/asciidoctor/asciidoctor/releases
:url-releases-asciidoctorjs: https://github.com/asciidoctor/asciidoctor.js/releases
:url-gitlab: https://gitlab.com
:url-issues: {url-repo}/issues
:url-milestone-3-2-0: {url-issues}?scope=all&state=closed&label_name%5B%5D=%5BVersion%5D%203.2.0
//:url-milestone-3-2-1: {url-issues}?scope=all&state=closed&label_name%5B%5D=%5BVersion%5D%203.2.1
:url-mr: {url-repo}/merge_requests

= Antora 3.2.0

_**Release date:** TBD | *Issue label:* {url-milestone-3-2-0}[3.2.0^]_

The primary focus of Antora {page-component-version} is to provide and refine certain API methods and endpoints to support the development of Antora extensions.
This release also added syntax for the inclusion and exclusion matching of worktrees and the ability to use the `ref` placeholder in the `edit_url` key.

You can find a summary of the issues resolved in this release below.

== Resolved issues

=== Added

Issue {url-issues}/535[#535^]:: Allow linked worktree to be used as content source; automatically resolve main repository location and remap HEAD reference in branches (content-aggregator).
Issue {url-issues}/1006[#1006^]::
* Use value of `versionSegment` property on component version in place of version in output path and URL of pages (content-classifier).
* Set dynamic `activeVersionSegment` property on component version to indicate which version segment is in use (content-classifier).
Issue {url-issues}/1008[#1008^]:: Add `addSplatAlias` method to `ContentCatalog` for adding a splat (i.e., directory) alias (content-classifier).
Issue {url-issues}/1013[#1013^]:: Add support for `ref` placeholder to insert full name of git ref (e.g., `refs/heads/v4.0.x`) in value of `edit_url` key on content source (content-aggregator).
Issue {url-issues}/1015[#1015^]::
* Emit `componentsRegistered` event after all components and versions have been registered (site-generator).
* Store raw files, nav, and startPage data on partially constructed component version until processed (content-classifier).
* Add readable property named `files` to component version in `ContentCatalog#registerComponentVersionStartPage` to get files for component version on access (content-classifier).
* Add readable property named `startPage` to component version in `ContentCatalog#registerComponentVersionStartPage` to look up start page for component version on access (content-classifier).
* Update `ContentCatalog#registerComponentVersionStartPage` to return start page (content-classifier).
Issue {url-issues}/1016[#1016^]:: Add syntax to match current branch of specific worktrees in branches pattern (for inclusion or exclusion) (e.g., `HEAD@5.8.x`) (content-aggregator).
Issue {url-issues}/1021[#1021^]:: Discover implicit site start page in component version promoted to site root (content-classifier).
Issue {url-issues}/1096[#1096^]:: Add `git.read_concurrency` playbook key to control how many git indexes to load into memory at once (playbook-builder).

=== Changed

Issue {url-issues}/1009[#1009^]:: Remove invalid trailing slash from value of `site.url` when building playbook and from value of `site.url` after `playbookBuilt` event (i.e., when it's provided by an extension) (playbook-builder).
Issue {url-issues}/1012[#1012^]:: Always assign the value `auth-required` to `origin.private` if the server requests credentials, even when the credentials are embedded in content source URL (content-aggregator).
Issue {url-issues}/1026[#1026^]:: Don't recompute resource ID on file from content aggregate if `src.family` is set (content-classifier).
Issue {url-issues}/1094[#1094^]::
* Fix relative path computation when path is extensionless and to matches parent folder of from (asciidoc-loader).
* Fix result of relativize helper when path is extensionless and to matches parent folder of from (page-composer).
Issue {url-issues}/1096[#1096^]::
* Change default value of `git.fetch_concurrency` to 1 (playbook-builder).
* Flag git clone/fetch error as recoverable if an unexpected network error occurs (content-aggregator).
* Decouple step to load (fetch or clone) repositories from step to scan repositories to discover references and start paths and collect files (content-aggregator).
Issue {url-issues}/1099[#1099^]::
* Preserve all information in wrapped error (content-aggregator, ui-loader, file-publisher)
Issue {url-issues}/1098[#1098^]::
* Upgrade isomorphic-git to incorporate patch for properly handling network error (content-aggregator)
* Add cause in log message when retrying failed fetch/clone operations in series (content-aggregator)
Issue {url-issues}/1101[#1101^]::
* Clear timeout and keep-alive settings on git HTTP connections that don't use custom agent (content-aggregator)
(No issue)::
* Don't read git tree twice if start path is empty; add dirname to tree object in same function (content-aggregator).
* Fill in progress bar with incomplete marks if cloning a repository fails (content-aggregator).
* Mention that credentials may have been rejected if server requests them, then sends 404 response (content-aggregator).
* Shorten `urlSegment` fragment in internal property names on ContentCatalog to `segment` (e.g., `latestVersionUrlSegment` -> `latestVersionSegment`) (content-classifier).
* Don't assign fallback value to `url` property on component version if property is already set (content-classifier).
* Print site URL instead of file URI in completion message if CI=true (site-generator).
* Change gulp-vinyl-zip dependency to @vscode/gulp-vinyl-zip (no functional changes) (ui-loader and file-publisher).
* Don't retry failed clone/fetch operation if playbook only has one content source URL (content-aggregator)

=== Fixed

Issue {url-issues}/1007[#1007^]:: Set `src.version` to original version segment and `src.rel.version` to actual version on splat alias file (content-classifier).
Issue {url-issues}/1010[#1010^]:: Fix infinite authorization loop if credentials embedded in the content source URL are empty and the repository requires authorization (content-aggregator).
Issue {url-issues}/1018[#1018^]:: Fix crash if value of `worktrees` key on content source is `~` (`null`) and at least one branch is specified (content-aggregator).
Issue {url-issues}/1020[#1020^]:: Add guard to prevent `ContentCatalog#registerSiteStartPage` from registering alias loop (content-classifier).
Issue {url-issues}/1022[#1022^]:: Decouple logic to compute default log format from process environment (playbook-builder).
Issue {url-issues}/1024[#1024^]:: Preserve target when creating static route if target is an absolute URL (redirect-producer).
Issue {url-issues}/1025[#1025^]:: Allow content aggregator to parse value of `content.branches` and `content.tags` playbook keys (playbook-builder).
Issue {url-issues}/1049[#1049^]:: Restore error stack in log output when using pino-std-serializers >= 6.1 (logger).
Issue {url-issues}/1070[#1070^]:: Add hostname to ignore list to prevent hostname property on logged error from modifying logger name (logger)
Issue {url-issues}/1064[#1064^]:: Consider local branches in non-managed bare repository that has at least one remote branch (content-aggregator).
Issue {url-issues}/1092[#1092^]:: Don't fail to load AsciiDoc if target of image macro resolves to an unpublished image (asciidoc-loader).
Issue {url-issues}/1095[#1095^]:: Retry loadUi in isolation after aggregateContent if network connection occurs when retrieving remote UI bundle (site-generator).
Issue {url-issues}/1096[#1096^]:: Retry failed fetch/clone operations in serial if git.fetch_concurrency > 1 and an unexpected error occurs (content-aggregator).
(No issue)::
* Use consistent formatting for error messages in playbook builder (playbook-builder).
* Correctly handle connection error when retrieving remote UI bundle (ui-loader).
* Don't fail to load AsciiDoc if pub or pub.moduleRootPath properties are not set on virtual file (asciidoc-loader).
* Look for IS_TTY on `playbook.env` in site generator to decouple check from process environment (site-generator).

[#thanks-3-2-0]
== Thank you!

Most important of all, a huge *thank you!* to all the folks who helped make Antora even better.
The {url-chat}[Antora community] has provided invaluable feedback and testing help during the development of Antora {page-component-version}.

We also want to call out the following people for making contributions to this release:

Raphael ({url-gitlab}/reitzig[@reitzig^]):: Fixing an error in the edit URL documentation ({url-mr}/940[!940^]).
{url-gitlab}/stoobie[@stoobie^]:: Documenting how to uninstall Antora globally resolves ({url-issues}/1014[#1014^]).

////
Gautier de Saint Martin Lacaze ({url-gitlab}/jabby[@jabby^])
Alexander Schwartz ({url-gitlab}/ahus1[@ahus1^])::
Andreas Deininger ({url-gitlab}/deining[@deining^])::
Ben Walding ({url-gitlab}/bwalding[@bwalding^])::
Daniel Mulholland ({url-gitlab}/danyill[@danyill^])::
Ewan Edwards ({url-gitlab}/eedwards[@eedwards^])::
George Gastaldi ({url-gitlab}/gastaldi[@gastaldi^])::
Germo Görtz ({url-gitlab}/aisbergde[@aisbergde^])::
Guillaume Grossetie ({url-gitlab}/g.grossetie[@g.grossetie^])::
Hugues Alary ({url-gitlab}/sturtison[@sturtison^])::
Jared Morgan ({url-gitlab}/jaredmorgs[@jaredmorgs^])::
Juracy Filho ({url-gitlab}/juracy[@juracy^])::
Marcel Stör ({url-gitlab}/marcelstoer[@marcelstoer^])::
Paul Wright ({url-gitlab}/finp[@finp^])::
Raphael Das Gupta ({url-gitlab}/das-g[@das-g^])::
Sturt Ison ({url-gitlab}/sturtison[@sturtison^])::
Vladimir Markiev ({url-gitlab}/Grolribasi[@Grolribasi^])::
////
